# Instruction for mods

This repo contains the mods (all jar files in this directory) for the TWoN modded server 1
Simply drag the mods files into the mods folder as appropriate depending on your system environment

* Windows
  * First install and run forge installer (client version) in the forge_installer directory
  * do windows+r
  * enter "appdata"
  * go to roaming/.minecraft/mods
  * drag jar files here

**Note that these mods are based off the forge version provided in the forge installer directory (MC 1.12.2)**

I also recommend the following (not required):
[Xaero's Minimap](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap/files/all?filter-game-version=2020709689%3A6756)
[Xaero's Worldmap](https://www.curseforge.com/minecraft/mc-mods/xaeros-world-map/files/all?filter-game-version=2020709689%3A6756)
[Ambience - Custom in-game audio tracks](https://www.curseforge.com/minecraft/mc-mods/ambience-music-mod/files/all?filter-game-version=1738749986%3a628)
